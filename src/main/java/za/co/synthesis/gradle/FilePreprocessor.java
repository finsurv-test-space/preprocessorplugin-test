package za.co.synthesis.gradle;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.gradle.api.GradleException;

/**
 * This class is taken straight from the project "jpp - A Simple Java Preprocessor for Ant" at https://github.com/abego/jpp
 * -- About jpp --
 * jpp is an Ant task for preprocessing text files, especially Java source files.
 * Its main purpose is to allow "conditional compilation". Other features commonly provided by text preprocessors,
 * like inclusion of files or macro expansion, are not supported.
 * Ant provides the jpp task with properties that control the preprocessing of the files. Using conditional directives
 * parts of the text file can be excluded in the resulting file.
 */
public class FilePreprocessor {
  private static class JavaFile {
    private File srcFile;
    private File destFile;

    public JavaFile(File srcFile, File destFile) {
      this.srcFile = srcFile;
      this.destFile = destFile;
    }

    public File getSrcFile() {
      return srcFile;
    }

    public File getDestFile() {
      return destFile;
    }
  }

  private static class JavaFileList extends ArrayList<JavaFile> {}


  public FilePreprocessor(Map<String, Object> properties) {
    this.providedProperties = properties;
  }

  private void walkFiles(String srcPath, String destPath, List<String> excludedDirs, JavaFileList fileList) {
    // If path is excluded then do not process the files
    boolean bExcludedPath = false;
    if (excludedDirs != null) {
      for (String excludedPath : excludedDirs) {
        if (srcPath.endsWith(excludedPath)) {
          bExcludedPath = true;
          break;
        }
      }
    }
    if (bExcludedPath)
      return;

    File root = new File(srcPath);
    File[] list = root.listFiles();

    if (list == null) return;

    for (File f : list) {
      if (f.isDirectory()) {
        String subPath = f.getName();
        walkFiles (srcPath + File.separator + subPath, destPath + File.separator + subPath, excludedDirs, fileList);
      }
      else {
        if (f.isFile() && f.getName().endsWith(".java")) {
          fileList.add(new JavaFile(f, new File(destPath + File.separator + f.getName())));
        }
      }
    }
  }

  public void preProcessFiles(String srcPath, String dstPath, List<String> excludedDirs) {
    JavaFileList fileList = new JavaFileList();

    walkFiles(srcPath, dstPath, excludedDirs, fileList);

    for (JavaFile file : fileList) {
      preProcessFile(file.getSrcFile(), file.getDestFile());
    }
  }

  public void preProcessFiles(List<String> srcPaths, String dstPath, List<String> excludedDirs) {
    JavaFileList fileList = new JavaFileList();

    for (String srcDir : srcPaths) {
      walkFiles(srcDir, dstPath, excludedDirs, fileList);
    }

    for (JavaFile file : fileList) {
      preProcessFile(file.getSrcFile(), file.getDestFile());
    }
  }

  public void preProcessFile(File srcFile, File destFile) throws RuntimeException {
    ensureDirExists(destFile.getParentFile());

    BufferedReader reader = null;
    BufferedWriter writer = null;
    try {
      reader = new BufferedReader(new FileReader(srcFile));
      writer = new BufferedWriter(new FileWriter(destFile));

      Parser parser = new Parser(reader, writer);
      parser.parse(true, false);

    } catch (Exception ex) {
      throw new RuntimeException(String.format(
              "Error when processing file '%s': %s",
              srcFile.getAbsolutePath(), ex.getMessage()), ex);

    } finally {
      if (reader != null) {
        try {
          reader.close();
        }
        catch (IOException e) {}
      }
      if (writer != null) {
        try {
          writer.close();
        }
        catch (IOException e) {}
      }
    }
  }

  // Parser ============================================================

  private static final String directivePrefixRE = "\\s*(?:(?:/\\*)|(?://))?#";
  private static final String directiveSuffixRE = "\\s*(?:#.*)?(?:\\*/)?\\s*";
  private static final String idRE = "[a-zA-Z_][a-zA-Z0-9_]*";
  private static final String conditionRE = "(\\!)?(" + idRE + ")";

  private static Pattern createDirectivePattern(String directiveRE) {
    return Pattern.compile(directivePrefixRE + directiveRE
            + directiveSuffixRE);
  }

  private static final Pattern re_if = createDirectivePattern("if\\s*(?:\\()?"
          + conditionRE + "(?:\\))?");
  private static final Pattern re_else = createDirectivePattern("else");
  private static final Pattern re_endif = createDirectivePattern("endif");

  private static final String newline = System.getProperty("line.separator");

  private class Parser {

    private BufferedReader reader;
    private BufferedWriter writer;
    private String line;
    private int lineNo = 0;

    private Parser(BufferedReader reader, BufferedWriter writer) {
      this.reader = reader;
      this.writer = writer;
    }

    private String readLine() throws IOException {
      line = reader.readLine();
      if (line != null) {
        lineNo++;
      }
      return line;
    }

    private Condition getIfCondition() {
      if (line == null) {
        return null;
      }
      Matcher m = re_if.matcher(line);
      return m.matches() ? new Condition(m.group(1) != null, m.group(2))
              : null;
    }

    private boolean isElseLine() {
      return line != null && re_else.matcher(line).matches();
    }

    private boolean isEndifLine() {
      return line != null && re_endif.matcher(line).matches();
    }

    public void parse(boolean copyLines, boolean insideIf)
            throws IOException {
      readLine();
      while (line != null) {
        Condition ifCondition = getIfCondition();
        if (ifCondition != null) {
          boolean isTrue = ifCondition.getValue();

          // process ifPart
          int ifLineNo = lineNo;
          parse(copyLines && isTrue, true);
          boolean hasElsePart = isElseLine();
          if (hasElsePart) {
            int elseLineNo = lineNo;
            parse(copyLines && !isTrue, true);

            if (!isEndifLine()) {
              throw new RuntimeException(String.format(
                      "Missing '#endif' for '#else' in line %d",
                      elseLineNo));
            }
          } else {
            if (!isEndifLine()) {
              throw new RuntimeException(String.format(
                      "Missing '#endif' for '#if' in line %d",
                      ifLineNo));
            }
          }
        } else if (isElseLine() || isEndifLine()) {
          if (!insideIf) {
            throw new RuntimeException(String.format(
                    "Missing '#if' for statement in line %d",
                    lineNo));
          }
          return;
        } else if (copyLines) {
          writer.write(line);
          writer.write(newline);
        }
        readLine();
      }
    }
  }

  private class Condition {
    private final boolean negate;
    private final String id;

    public Condition(boolean negate, String id) {
      this.id = id.trim();
      this.negate = negate;
    }

    public boolean getValue() {
      Object value = getPropertyValue(id);
      boolean isTrue = getBooleanValue(value);
      return negate ? !isTrue : isTrue;
    }
  }

  // Properties ============================================================

  private Map<String, Object> providedProperties;

  private Object getPropertyValue(String name) {
    return providedProperties.get(name);
  }

  // Util ============================================================

  private static void ensureDirExists(File dir) {
    if (!dir.exists()) {
      if (!dir.mkdirs()) {
        throw new GradleException("Cannot create directory: "
                + dir.getAbsolutePath());
      }
    }
  }

  private static boolean getBooleanValue(Object value) {
    if (value == null) {
      return false;
    }
    if (value instanceof Boolean) {
      return ((Boolean) value).booleanValue();
    } else {
      return value.toString().equalsIgnoreCase("true");
    }
  }
}