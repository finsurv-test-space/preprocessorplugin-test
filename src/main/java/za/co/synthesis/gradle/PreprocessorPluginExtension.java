package za.co.synthesis.gradle;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PreprocessorPluginExtension {
  private List<String> srcDirs = Arrays.asList("src");
  private final String preprocessorOutput = "build/preprocessed";
  private List<String> excludeDirs = Arrays.asList();
  private Map<String, Object> directives = new HashMap<String, Object>();

  public PreprocessorPluginExtension() {
    directives.put("OLDDATE", true);
  }

  public String getPreprocessorOutput() {
    return preprocessorOutput;
  }

  public List<String> getSrcDirs() {
    return srcDirs;
  }

  public void setSrcDirs(List<String> srcDirs) {
    this.srcDirs = srcDirs;
  }

  public Map<String, Object> getDirectives() {
    return directives;
  }

  public void setDirectives(Map<String, Object> directives) {
    this.directives = directives;
  }

  public List<String> getExcludeDirs() {
    return excludeDirs;
  }

  public void setExcludeDirs(List<String> excludeDirs) {
    this.excludeDirs = excludeDirs;
  }
}