package za.co.synthesis.gradle;

import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.tasks.SourceSet;

import java.io.File;
import java.util.Arrays;

public class PreprocessorPlugin implements Plugin<Project> {
  @Override
  public void apply(Project project) {
    PreprocessorPluginExtension extension =
            project.getExtensions().create("preprocessSetting", PreprocessorPluginExtension.class);
    project.getTasks().create("preprocess", PreprocessorTask.class);

    project.getPlugins().withType(JavaPlugin.class, new Action<JavaPlugin>() {
      public void execute(JavaPlugin javaPlugin) {
        JavaPluginConvention javaConvention =
                project.getConvention().getPlugin(JavaPluginConvention.class);
        SourceSet main = javaConvention.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME);
        main.getJava().setSrcDirs(Arrays.asList(extension.getPreprocessorOutput() + File.separator + "main"));

        SourceSet test = javaConvention.getSourceSets().getByName(SourceSet.TEST_SOURCE_SET_NAME);
        test.getJava().setSrcDirs(Arrays.asList(extension.getPreprocessorOutput() + File.separator + "test"));
      }
    });
  }
}

