package za.co.synthesis.gradle;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;


public class PreprocessorTask extends DefaultTask {
  @TaskAction
  public void preprocess() {
    PreprocessorPluginExtension extension = getProject().getExtensions().findByType(PreprocessorPluginExtension.class);
    if (extension == null) {
      extension = new PreprocessorPluginExtension();
    }

    FilePreprocessor preprocessor = new FilePreprocessor(extension.getDirectives());
    preprocessor.preProcessFiles(extension.getSrcDirs(), extension.getPreprocessorOutput(), extension.getExcludeDirs());
  }
}