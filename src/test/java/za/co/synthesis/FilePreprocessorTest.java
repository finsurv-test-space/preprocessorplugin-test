package za.co.synthesis;

import junit.framework.AssertionFailedError;
import org.gradle.internal.impldep.org.apache.commons.io.FileUtils;
import org.junit.Test;
import za.co.synthesis.gradle.FilePreprocessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;

public class FilePreprocessorTest {
  @Test
  public void preprocess_olddate_test() throws IOException {
    Map<String, Object> directives = new HashMap<String, Object>();

    directives.put("OLDDATE", true);

    FilePreprocessor preprocessor = new FilePreprocessor(directives);

    File srcFile = new File("./src/test/resources/FilePreprocessorTestSrc.java");
    File destFile = new File("./build/generated/FilePreprocessorTestOut.java");

    preprocessor.preProcessFile(srcFile, destFile);

    File outFile1 = new File("./src/test/resources/FilePreprocessorTestOut1.java");
    assertTrue("preprocessor OLDDATE=true output invalid",  FileUtils.contentEquals(destFile, outFile1));
  }

  @Test
  public void preprocess_newdate_test() throws IOException {
    Map<String, Object> directives = new HashMap<String, Object>();

    directives.put("OLDDATE", false);

    FilePreprocessor preprocessor = new FilePreprocessor(directives);

    File srcFile = new File("./src/test/resources/FilePreprocessorTestSrc.java");
    File destFile = new File("./build/generated/FilePreprocessorTestOut.java");

    preprocessor.preProcessFile(srcFile, destFile);

    File outFile1 = new File("./src/test/resources/FilePreprocessorTestOut2.java");
    assertTrue("preprocessor OLDDATE=false output invalid",  FileUtils.contentEquals(destFile, outFile1));
  }

  private static void verifyDirsAreEqual(Path one, Path other) throws IOException {
    Files.walkFileTree(one, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file,
                                       BasicFileAttributes attrs)
              throws IOException {
        FileVisitResult result = super.visitFile(file, attrs);
        if (file.toString().endsWith(".java")) {
          // get the relative file name from path "one"
          Path relativize = one.relativize(file);
          // construct the path for the counterpart file in "other"
          Path fileInOther = other.resolve(relativize);

          byte[] otherBytes = Files.readAllBytes(fileInOther);
          byte[] thisBytes = Files.readAllBytes(file);
          if (!Arrays.equals(otherBytes, thisBytes)) {
            throw new AssertionFailedError(file + " is not equal to " + fileInOther);
          }
        }
        return result;
      }
    });
  }

  @Test
  public void preprocess_dir_olddate_test() throws IOException {
    Map<String, Object> directives = new HashMap<String, Object>();

    directives.put("OLDDATE", true);

    FilePreprocessor preprocessor = new FilePreprocessor(directives);

    String generatedDir = "./build/generated/olddate";

    FileUtils.deleteDirectory(new File(generatedDir));

    preprocessor.preProcessFiles("./src/test/resources/java", generatedDir, Arrays.asList("./src/test/resources/java/za/co/synthesis/rule/newdate"));

    verifyDirsAreEqual(Paths.get("./src/test/resources/olddate"), Paths.get(generatedDir));
  }
}
