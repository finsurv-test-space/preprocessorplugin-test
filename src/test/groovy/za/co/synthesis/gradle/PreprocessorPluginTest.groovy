package za.co.synthesis.gradle

import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project

import static org.junit.Assert.*

class PreprocessorPluginTest {
    @Test
    public void preprocessor_plugin_should_add_task_to_project() {
        Project project = ProjectBuilder.builder().build()
        project.getPlugins().apply 'synthesis.gradle.preprocess.plugin'

        assertTrue(project.tasks.preprocess instanceof PreprocessorTask)
    }
}
