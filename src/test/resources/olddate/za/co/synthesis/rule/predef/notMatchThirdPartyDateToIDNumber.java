package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import za.co.synthesis.rule.support.legacydate.LocalDate;

/**
 * User: jake
 * Date: 8/9/14
 * Time: 7:26 PM
 * Determines if the current monetary amount's third party individual ID number does not start with the third
 * party individual date of birth
 */
public class notMatchThirdPartyDateToIDNumber implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    LocalDate date = Util.date(context.getMoneyField(context.getCurrentMoneyInstance(), "ThirdParty.Individual.DateOfBirth"));
    Object idObj = context.getMoneyField(context.getCurrentMoneyInstance(), "ThirdParty.Individual.IDNumber");

    return idObj == null || !Util.doesDateMatchSAID(date, idObj.toString());
  }
}
/*
function notMatchThirdPartyDateToIDNumber(context, value) {
  // we won't be using this field's value, we're matching other fields
  var dateStr = context.getMoneyField(context.currentMoneyInstance, "ThirdParty.Individual.DateOfBirth");
  var idStr = context.getMoneyField(context.currentMoneyInstance, "ThirdParty.Individual.IDNumber");
  if (notEmpty(context, dateStr) && notEmpty(context, idStr)) {
    dateStr = dateStr.trim();
    idStr = idStr.trim();
    if(dateStr.length >= 10 && idStr.length >= 6) {
      if(dateStr[2] == idStr[0] && dateStr[3] == idStr[1] &&
         dateStr[5] == idStr[2] && dateStr[6] == idStr[3] &&
         dateStr[8] == idStr[4] && dateStr[9] == idStr[5]) {
        return false;
      }
    }
  }
  return true;
}
*/

