package za.co.synthesis.rule.predef;

import za.co.synthesis.rule.support.FinsurvContext;
import za.co.synthesis.rule.support.IAssertionFunction;
import za.co.synthesis.rule.support.Util;

import za.co.synthesis.rule.support.legacydate.LocalDate;

/**
 * User: jake
 * Date: 8/9/14
 * Time: 7:21 PM
 * Determines if the resident individual ID number does not start with the resident individual date of birth
 */
public class notMatchResidentDateToIDNumber implements IAssertionFunction {
  public boolean execute(FinsurvContext context, Object value) {
    LocalDate date = Util.date(context.getTransactionField("Resident.Individual.DateOfBirth"));
    Object idObj = context.getTransactionField("Resident.Individual.IDNumber");

    return idObj == null || !Util.doesDateMatchSAID(date, idObj.toString());
  }
}
/*
function notMatchResidentDateToIDNumber(context, value) {
  // we won't be using this field's value, we're matching other fields
  var dateStr = context.getTransactionField("Resident.Individual.DateOfBirth");
  var idStr = context.getTransactionField("Resident.Individual.IDNumber");
  if (notEmpty(context, dateStr) && notEmpty(context, idStr)) {
    dateStr = dateStr.trim();
    idStr = idStr.trim();
    if(dateStr.length >= 10 && idStr.length >= 6) {
      if(dateStr[2] == idStr[0] && dateStr[3] == idStr[1] &&
         dateStr[5] == idStr[2] && dateStr[6] == idStr[3] &&
         dateStr[8] == idStr[4] && dateStr[9] == idStr[5]) {
        return false;
      }
    }
  }
  return true;
}
*/
