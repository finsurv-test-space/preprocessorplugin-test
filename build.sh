#!/bin/bash

#set -eE
ROOT=$PWD

write_metadata(){
   echo "{
                \"artefact\":
                [{
                        \"name\":\"${ROOT}/build/libs/*.jar\",
                        \"destination\":\"NONE\"
                }]
        }" >> ${ROOT}/build/metadata.json
}

rm -rf ${ROOT}/build

./gradlew clean
./gradlew publishToMavenLocal

write_metadata

# if [ ! -d "BuiltFiles" ]
# then
# 	mkdir "BuiltFiles"
# fi

# rm -f "BuiltFiles/*"
# cp "build/libs/"*.jar "BuiltFiles/"
# rm -f "BuiltFiles/"*source*.jar